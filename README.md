# SkilStak Flappy Boilerplate

Fork or just copy the `index.html` and `skilstak-flappy.js` into your
own `gh-pages` repo or main `<you>.github.io` site. Don't forget to
make your own [`assets`](assets/).

Experiment with "hacking" the game by changing the CONFIG settings
at the beginning of the skilstak-flappy.js file, but be careful not
to break it and check for syntax errors.  This is free and unencumbered
software released into the public domain.

## Image Asset Dimensions

* `background.png` - 960 x 568 (3x game width so scrolls)
* `button.png` - whatever
* `flappy.png` - 46 x 64 (2 frames)
* `pipebot.png` - 90 x 500
* `pipetop.png` - 90 x 500
* `sign.png` - 46 x 64 (2 frames)
* `skilstak.png`

## Sound Assets

* `gameover.wav`
* `point.wav`
* `start.mp3`
* `flap.wav`
* `crash.wav`
* `play.mp3`

## PUBLIC DOMAIN LICENSE

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org>

